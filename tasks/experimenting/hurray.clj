(ns experimenting.hurray)

;;; we have moved to dep.edn an option from the command lime call: clojure -m clojurep.hurray
;;; now we can call this function with: clj -A:tasker

(defn -main []
  (println "Main version of hurray, so perhaps we can use shell\nor other language to call script and then use Clojure language"))
